/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//FORM VALIDATION
$(window).load(function(){
    
    formValidation.init();
    
        });
        
var formValidation = {
    
    init : function(){
        
        var self = this;
        
        var $forms = $('form[data-validate=true]');
        
        this.addNotification($forms);
    
        $forms.submit(function()
        {
        
            var e = [];
            
            var errorString = [];
            
            var formId = $(this).attr('id');
            
            var $notificationBox = $(this).find('.notification').eq(0);
            
            Validation.init($(this));
            
            $notificationBox.hide().empty();//hide notificaiton box
            //
            //validate text, password and textarea fields
            $(this).find(':text,textarea,:password').each(function()
            {
                var value = $(this).val();
                var id = Validation.attachId($(this));
                var errormessage = $(this).attr('data-emessage');
                var input_name = $(this).attr('name');
                var required = $(this).attr('data-required');
                var alternative_entry = $(this).data('no-validate-if-set');

                // Only validate if we haven't been told to ignore if another
                // field has been provided...
                if (alternative_entry !== undefined)
                {
                    if ($('#' + alternative_entry).val())
                    {
                        return;
                    }
                }
                
                //only validate if there is a value inputed
                if(!value.length && required === 'false'){
                    return;
                }

                if($(this).attr('data-min'))
                {

                    var minLength = $(this).attr('data-min');
                    var em = errormessage || 'must be at least '+minLength+ ' characters';
                    if(value.length < minLength)
                    {
                        e[e.length] = [id,value, em ];
                        self.addError(errorString,input_name, em);
                    }
                }

                if($(this).attr('data-max'))
                {

                    var maxLength = $(this).attr('data-max');
                    var em = errormessage || 'must not exceed '+ maxLength+ ' characters';
                    if(value.length > maxLength)
                    {
                        e[e.length] = [id,value, em];
                        self.addError(errorString,input_name,em);
                    }
                }

                if($(this).attr('data-match') && value.length>0)
                {

                    var pattern = $(this).attr('data-match');
                    var regexMessage = $(this).attr('data-regXmessage');
                    var em = errormessage || 'Only alphanumeric, underscore and dash allowed';
                    var em = regexMessage || em;
                    var regex = new RegExp(pattern, 'i');
                    var tt = value.match(regex);
                    if (tt===null)
                    {
                        e[e.length] = [id, value, em];
                        self.addError(errorString,input_name,em);
                    }
                }
                
                if($(this).attr('data-not-match') && value.length>0)
                {

                    var pattern = $(this).attr('data-not-match');
                    var regexMessage = $(this).attr('data-regXmessage');
                    var em = errormessage || 'Only alphanumeric, underscore and dash allowed';
                    var em = regexMessage || em;
                    var regex = new RegExp(pattern, 'i');
                    var tt = value.match(regex);
                    if (tt!==null)
                    {
                        e[e.length] = [id, value, em];
                        self.addError(errorString,input_name,em);
                    }
                }

            });

            //validate select
            $(this).find('select').each(function(){

                var value = $(this).val();
                var id = Validation.attachId($(this));
                var errormessage = $(this).attr('data-emessage');
                var input_name = $(this).attr('name');
                var required = $(this).attr('data-required');
                 
                //only validate if there is a value inputed
                if(!value.length && required === 'false'){
                    return;
                }
                
                var em = errormessage || 'not selected';
                

                if(!value) 
                {

                    e[e.length] = [id, value, em ];
                    self.addError(errorString,input_name,em);

                }
            });



        //Display errors
            if(e.length)
            {//if there is an error, prevent form from submitting           
                var f = Validation.convertToExpectedFormat(e); 
                
                var errorstring = errorString.join('</div><div>');
                
                $notificationBox.append('<div>'+errorstring+'</div>').show();

                Validation.displayError(f);

                return false;
            }
            
        });
        
        
    
    },
    
    addError : function (arr, name, em){
        
        arr[arr.length] = this.ucfirst(name.replace('_',' ')) + ' ' +em.toLowerCase();
    },
    
    ucfirst : function (str) {
      //  discuss at: http://phpjs.org/functions/ucfirst/
      // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      // bugfixed by: Onno Marsman
      // improved by: Brett Zamir (http://brett-zamir.me)
      //   example 1: ucfirst('kevin van zonneveld');
      //   returns 1: 'Kevin van zonneveld'

      str += '';
      var f = str.charAt(0)
        .toUpperCase();
      return f + str.substr(1);
    },
    
    addNotification : function($forms){
        
            $forms.each(function(){
                
                var id = Validation.attachId($(this));
                
                $(this).prepend('<div id="notification-'+id+'" class="notification alert alert-danger" style="display:none"></div>');
                
                $(this).click(function(e){
                    if($(e.target).hasClass('error')){
                        Validation.removeError($(e.target).parent());
                    }
                });
            });
            
        
        }
  
};