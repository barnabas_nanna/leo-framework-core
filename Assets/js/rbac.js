/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    $(window).load(function(){
        $('.quick-nav').hide();
    });
    
    jQuery.fn.toggleAttr = function (attr,attr2) {
        return this.each(function () {
            var $this = $(this);
            $this.attr(attr) ? $this.removeAttr(attr) : $this.attr(attr, attr2);
        });
    };
    
    var Rbac = {
        action : 'rbac',//url where ajax call should be made to
        data : {},
        routes : {},
        groups : {},
        UsersIds : [], //userIds selected to be added to a group
        responseData : '', //response data from server if all goes well
        ajaxStatus : true, //allow ajax query
        init: function (data)
        {
            this.setRoutes(data.routes);
            this.setGroups(data.groups);
            this.activateToggler();
            this.attachEvents();            
            
        },
        attachEvents: function(){
            var self = this;
            this.attachCreateGroup();
            this.attachLeafEvents();
            this.attachCreateRouteGroupEvent();
            this.attachRouteGroupEvents();
            this.attachGroupUserEvents();
            this.attachAddGroupUserEvent();
            this.attachRouteGroupGranularityEvent();
            this.attachClearSession();
            this.attachPager();
            this.attachDeleteGroup();
            this.attachRouteFilter();
                    
            $('button.clearSelected').click(function(){
                var $section = $(this).parents('section').eq(0);
                $section.find('.selected').removeClass('selected').removeAttr('data-selected');
                $section.find('.inner-list').slideUp().find('li').css('color','#857c7a');
                $section.find('.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                if($section.attr('id')==='groups_users')
                {//empty selected users
                    self.UsersIds = [];
                }
            });
            
            
        },
        attachPager: function()
        {
           var self = this;
           
           $('.paginations button').click(function(e){
               var $target = $(this);
               $('#groups_users .errorMessage').empty();//remove error message
               if(!$target.attr('class').length)
               {
                   return;
               }
               var direction = $target.attr('class');
               var lastId = $target.attr('data-lastid');
               var filter = $target.parents('section').eq(0).find('input').val();
              
               var data = {task:'pagination', direction:direction, filter:filter, lastId:lastId};
               self.updateData(data);
               
               self.submitForm();
               
           });
           
           $('#groups_user_input').change(function()
           {//if the filter is changed, reset the last id 
               $('#groups_users .paginations').find('button').attr('data-lastid',0);
           }).keyup(function(e){
             
           if(e.keyCode==13){//click next button if the Return button is pressed
                 
                    if($(this).val().length>2)
                    {
                        $('#groups_users .paginations').find('button:eq(0)').click();
                    }
                    
                }
           });
        },
        updateUserTree : function (data)
        {
            var $pager = $('#groups_users .paginations').eq(0);
            if(!data.tree.length)
            {
                
                $pager.find('.errorMessage').html('No users found').css('color','red');
                return;
            }
            if(data.lastId>0)
            {
                $pager.find('button:eq(0)').attr('data-lastid',data.lastId);
            }
            if(data.nextId>0)
            {
                $pager.find('button:eq(1)').attr('data-lastid',data.nextId);
            }
                
            
            $('#groups_users ul').html(data.tree);
            
        },
        
        attachClearSession : function(){
            var self = this;
            $('#clearSession').click(function(){
                //Attempt to create new group
            var data = {task:'clear_session'};
            
                self.updateData(data);
               
                self.submitForm();
                
            });
        },
        
        attachDeleteGroup : function(){
            
             var self = this;
             
          $('#delete_group').click(function(){
              
              if(!confirm('Are you sure? It is not reversible'))
                 {
                     return;
                 }
                 
                 
              if(!confirm('Confirm deletion?'))
               {
                    return;
               }
                                  
                var groups = $('#group-lists').find('[data-selected=selected]');
                
               
                if(groups.length)
                {    
                    
                    var GroupIds =[];
                    
                    groups.each(function(){
                        GroupIds[GroupIds.length] = $(this).attr('id');
                        
                    });
                    
                    var data = {
                      task: 'delete_groups',
                      groups : GroupIds
                    };

                    self.updateData(data);
                    self.submitForm();
                    
                }else{
                    
                    if(!groups.length)
                    {
                        self.alertify('No groups selected',false);
                    }
                }
            });
            
        },
        attachCreateGroup : function (){
            var self = this;
            
              //attach event for creating groups
             $('#create_group').on('click',function(){
                 
                var $groupText = $('#groups_input');
                var groupName = $groupText.val();
                if(!groupName.trim().length)
                {
                    
                    self.errorInput($groupText);
                    
                }else{
                    
                    self.createGroup(groupName);
                    
                }
                
            });
        },
        
        activateToggler : function (className){ //activate the span used to toggle list
    
            $('li .toggle').click(function(){
                                
                $(this).toggleClass('open').siblings('ul.inner-list').slideToggle();
                $(this).find('.fa').toggleClass('fa-chevron-down fa-chevron-up');
                
            });
        },
        
        setGroups : function (groups){
            this.groups = groups;
        },
        
        setRoutes : function(routes){
            this.routes = routes;
        },
        
        addGroup : function(groupName){
            this.groups[this.groups.length] = groupName;
        },
        
        createGroup : function(groupName){
            // exists check case sensitive search
            var index = this.getIndex(this.groups, groupName.toLowerCase(),true);
            
            if(index!==-1)
            {
                this.alertify(groupName + ' already exist',false);
                
               return;
            }
            
            //Attempt to create new group
            var data = {gname : groupName, task:'create_group'};
            
               this.updateData(data);
               this.submitForm();
               
               
                        
        },
        
        getIndex: function (haystack,value,caseinsensitive)
        {
            if(!caseinsensitive)
            {
                return haystack.indexOf(value);
                
            }else
            {//if case insensitive search
                for (var i in haystack)
                {
                    if(value.toLowerCase() === haystack[i].toLowerCase())
                    {
                        return i;
                    }
                }
            }
            
            return -1;
            
        },
        
        updateData : function(data){
            
            this.data = data;
        },
        
        updateAction : function(url){
            if(!url.length){
                this.alertify('url is an empty string',false);
                throw 'Please provide a valid url';
            }
            this.action = url;
        },
        
        submitForm : function(){
            var self = this;            
            
            //prevent any other ajax query to go through till last one completes
            this.ajaxStatus = false;
            
            $.ajax({
                dataType : 'json',
                url   : this.action,
                data  : this.data,
                type  : 'POST',
                always: function(data)
                {//store last response data incase it wants to be used
                    
                  this.responseData = data; 
                  
                },
                success: function(data)
                {
                    if(!data.success)
                    {
                        
                      self.alertify(data.reason,false);
                      
                    }else{
                                                
                        if(data.callback)
                        {// there is a callback function, pass the response to handle it
                            Rbac[data.callback](data);
                        }
                        
                        return true;
                                                
                    }
                },
                fail: function(data)
                {
                 
                   self.alertify('An unknown error occurred.',false); 
                   
                }
            });
            
            
        },
        updateGroup: function(data){
           var $e = $('#groups').find('.middle-section ul');
           
           $e.append('<li id="'+data.groupId+'" class="group_name">'+data.group_name+'</li>');
            $('#groups_input').val('');//clear group name
            
            this.addGroup(data.group_name);

        },
        
        errorInput: function(objectRef){
            objectRef.css({'border' : '1px solid red'});
        },
        convArrToObj: function(array){
            var thisEleObj = {};
            if(typeof array === "object"){
                for(var i in array){
                    var thisEle = convArrToObj(array[i]);
                    thisEleObj[i] = thisEle;
                }
            }else {
                thisEleObj = array;
            }
            return thisEleObj;
        },
        alertify : function(message, messageType)
        {
            alert(message);
        },
        attachLeafEvents : function ()
        {
            $('#routes').find('ul.inner-list').click(function(e){
                var $target = $(e.target);
                if($target.hasClass('leaf'))
                {
                    
                     $target.toggleClass('selected').toggleAttr('data-selected','selected');
                    
                }
            });
            
        },
        attachGroupUserEvents : function ()
        {
            var self = this;
            $('#groups_users').find('ul').click(function(e){
                var $target = $(e.target);
                if($target.hasClass('leaf'))
                {
                    $target.toggleAttr('data-selected','selected').toggleClass('selected');
                    var id = $target.attr('id');
                    var index = self.getIndex(self.UsersIds, id);
                    if(index === -1 ){//add userid
                        self.UsersIds[self.UsersIds.length] = id;
                    }else{//remove userid
                        self.UsersIds.splice(index,1);
                    }
                }
            });
            
        },
        attachRouteGroupEvents : function()
        {
            $('ul#group-lists').click(function(e){
                var $target = $(e.target);
                if($target.hasClass('group_name'))
                {
                    $target.toggleAttr('data-selected','selected').toggleClass('selected');
                }
            });
        },//route_group_leaf
        attachRouteGroupGranularityEvent: function()
        {
            var self = this;
            
            $('ul#route_group_leaf').click(function(e){
                var $target = $(e.target);
                if($target.hasClass('route_group_leaf'))
                {
                    $target.toggleAttr('data-selected','selected').toggleClass('selected');
                    $target.find('.showRules').remove();
                }
                
                if($target.hasClass('rules')){
                    e.stopPropagation();
                    var liParent = $target.parents('li').eq(0);
                    var rules = liParent.attr('data-rules');
                    var id = liParent.attr('data-id');
                    liParent.find('.showRules').remove();
                    if(rules.length)
                    {
                        liParent.append('<div class="showRules">'+self.addDisplayDelete(rules,id)+'</div>');
                    }
                    
                }
                
                if($target.hasClass('denyAcess'))
                {
                    if(confirm('Deny page access'))
                    {
                        e.stopPropagation();
                        var liParent = $target.parents('li').eq(0);
                        var id = liParent.attr('data-id');
                        var data = {task : 'delete_page_access', route_group_id : id};
                        self.updateData(data);
                        self.submitForm();
                        liParent.remove();
                    }
                }
                
                if($target.hasClass('deleteGranular')){
                    e.stopPropagation();
                    var id = $target.attr('data-id');
                    var granularRule = $target.attr('data-rule').trim();
                    $target.parents('li.route_group_leaf').attr('data-rules').replace(granularRule,'');
                    $target.parents('li').eq(0).remove();//remove from dom
                    var data = {task : 'delete_granularity', id : id , granularity : granularRule};
                    self.updateData(data);
                    self.submitForm();
                    
                }
            });
            
            $('#addGranular').click(function(){
                var value = $('#routes_groups_input').val();
                var groups = $('ul#route_group_leaf').find('[data-selected=selected]');
                if(groups.length && value.length)
                {    
                    
                    var GroupIds =[];
                    var GranularRules = [];
                    
                    groups.each(function(){
                        GroupIds[GroupIds.length] = $(this).attr('data-id');
                        GranularRules[GranularRules.length] = $(this).attr('data-rules');
                    });
                    
                    var data = {
                      task: 'new_group_granular_rule',
                      value : value,
                      route_group_ids : GroupIds,
                      granular :  GranularRules
                    };

                    self.updateData(data);
                    self.submitForm();
                    
                }else{
                    
                    if(!value.length)
                    {
                        self.alertify('No Rules added',false);
                    }
                    
                    if(!groups.length)
                    {
                        self.alertify('No groups selected',false);
                    }
                }
            });
            
        },
        updatePageAcess: function(data){//callback to handle when a group has been denied page access
            
        },
        
        updateGranularRuleForGroup : function(data){//callback when a granular rule is deleted
            //location.reload();
        },
        attachAddGroupUserEvent : function(){
             var self = this;
          $('#addGroupUser').click(function(){
              
                var groups = $('ul#group-lists').find('[data-selected=selected]');
                var UserIds = self.UsersIds;
               
                if(groups.length && UserIds.length)
                {    
                    
                    var GroupIds =[];
                    
                    groups.each(function(){
                        GroupIds[GroupIds.length] = $(this).attr('id');
                        
                    });
                    
                    var data = {
                      task: 'new_group_user',
                      users : UserIds,
                      groups : GroupIds
                    };

                    self.updateData(data);
                    self.submitForm();
                    
                }else{
                    
                    if(!UserIds.length)
                    {
                        self.alertify('No users selected',false);
                    }
                    
                    if(!groups.length)
                    {
                        self.alertify('No groups selected',false);
                    }
                }
            });
            
        },
        attachCreateRouteGroupEvent : function()
        {
            var self = this;
            
            $('#addRouteGroup').click(function(){
                var routes = $('#routes').find('ul.inner-list').find('[data-selected=selected]');
                var groups = $('ul#group-lists').find('[data-selected=selected]');
               
                if(groups.length && routes.length)
                {    
                    var RoutesPath = [];
                    var GroupIds =[];
                    
                    routes.each(function(){
                        RoutesPath[RoutesPath.length] = $(this).attr('data-route');
                    });
                    
                    groups.each(function(){
                        GroupIds[GroupIds.length] = $(this).attr('id');
                    });
                    
                    var data = {
                      task: 'new_route_group',
                      routes : RoutesPath,
                      groups : GroupIds
                    };

                    self.updateData(data);
                    self.submitForm();
                    
                }else{
                    
                    if(!routes.length)
                    {
                        self.alertify('No routes selected',false);
                    }
                    
                    if(!groups.length)
                    {
                        self.alertify('No groups selected',false);
                    }
                }
            });
            
        },
        updateRouteGroupTree : function(data)
        {
            //alert('update route group tree');
            location.reload();
        },
        updateGroupUserTree:function(data)
        {
            location.reload();
            //alert('update  group user tree');
        },
        updateGroupGranularTree : function(data)
        {
            location.reload();
            //alert('update  group granularity rule tree');
        },
        addDisplayDelete : function(accessRules,id){
            var rules = accessRules.split(" ");
            var ruleString = '';
            for (var i in rules)
            {
                var rule= rules[i].trim();
                if(rule.length)
                {
                    ruleString += '<li><span>'+rules[i]+'</span> <small><span class="deleteGranular" data-id="'+id+'" data-rule="'+rule+'">delete</span></small></li>';
                }
                
            }
            
            return '<ul>'+ruleString+'</ul>';
        },
        updateGroupTree : function(){//remove deleted groups from dom
            $('#group-lists').find('[data-selected=selected]').remove();
        },
        attachRouteFilter : function()
        {
            $('#routes_text').keyup(function(){
                var val = $(this).val();
                if(val.length < 2) {return;}
                var t = $( "#routes .inner-list li:contains('"+val+"')" );
                        t.css( "color", "blue" );
                        var $section = t.parent().slideDown().parent();
                        $section.find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }).blur(function(){
                var val = $(this).val();
                 $( "#routes .inner-list li:not(:contains('"+val+"'))" ).css( "color", "#857c7a" );
            });
        }
        
    };