<?php
namespace Leo\Models;

/**
 * Description of Me
 *
 * @author barnabasnanna
 */
class Me extends \Leo\ObjectBase
{
    
    private $userId;
    private $email;
    private $fullname;
    private $data =[];
    private $domain_id = 0;
    private $country_id = 0;
    
    const ME = '_logged_in_user_';
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function getFullName()
    {
        return $this->fullname;
    }
    
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    public function setUserId($id)
    {
        $this->userId = $id;
        return $this;
    }
    
    public function getUserId()
    {
        return $this->userId;
    }
    

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    public function getDomainId()
    {
        return $this->domain_id;
    }

    public function setDomainId($domain_id)
    {
        $this->domain_id = $domain_id;
        return $this;
    }
    
    /**
     * Return a value from storage
     * @param string $key
     * @param mixed $default
     * @return mixed value if set or provided default value or null
     */
    public function get($key, $default = null)
    {
        
        if(!empty($this->data[$key]))
        {
            return $this->data[$key];
        }
        
        return $default;
    }
    
    /**
     * Add a value to data storage
     * @param string $key
     * @param mixed $value
     * @param bool $overwrite should the new value overwrite the any existing value
     */
    public function add($key, $value, $overwrite = false)
    {
        
        if(empty($this->data[$key]) )
        {
            $this->data[$key] = $value;
        }
        elseif($overwrite)
        {
            $this->remove($key);
            $this->add($key, $value);
        }
        
    }

    protected function remove($key)
    {
        
        if(!empty($this->data[$key]) )
        {
            unset($this->data[$key]);
        } 
    }

    /**
     * Return identity information of the logged in user
     * @return Me
     */
    public static function getInstance()
    {
        if(PHP_SAPI === 'cli') {
            return 0;
        }

        return leo()->getSession()->get(self::ME);
    }
    
    public static function isGuest()
    {
        return !leo()->getSession()->exists(self::ME);
    }

    /**
     * GEt the country associated to a domain
     * @return int|\Leo\Db\ActiveRecord|null
     */
    public function getCountryId()
    {
        if(!$this->country_id)
        {
            $sql = "SELECT country_id FROM domains WHERE domain_id = ".$this->getDomainId();
            $this->country_id = leo()->getDb()->run($sql)->getFirst();
        }
        return $this->country_id;
    }
    
    
}
